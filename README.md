# noroff-assignment-002-data-persistence-and-access

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

A solution for the acadmy assignment 002-data-persistence-and-access

## General Information

This is the final version of the submitted repository for the assignment.
It was last altered by Daniel Ruegenberg.
Every indivudal submit, that links to this repositry, was meant to to point to this final version of the repository.
If the date of an indivdual submit is older than the last update of this repository, that is because this team member had finished his duty for the assignment.
This repository will not be changed after every team member has submitted.

Team members:

	Rick Schnellert
	Daniel Ruegenberg


## Table of Contents

- [Install](#install)
- [Usage](#usage)
- [Maintainers](#maintainers)
- [License](#license)

## Install

```
First part:
	Execute the .sql files in folder Databanks-part1. Alternatively you can open them in a text editor, copy the whole text and paste it into a new query in SSMS.
	WARNING: The files are to be executed in order to work, as they depent on another.
	
Second part:
	Clone the repositroy and open the solution (.sln) file located in folder Databanks-part2 in Visual Studio
```

## Usage

```
First part:
	Execute the .sql files in folder Databanks-part1. Alternatively you can open them in a text editor, copy the whole text and paste it into a new query in SSMS.
	WARNING: The files are to be executed in order to work, as they depent on another.
	
Second part:
	The implemented methods are to be used in the main function in Program.cs
	For concrete documentation of the individual methods look at /Repositories/CustomerRepository.cs
	Unit tests for each method are provided in the Program.cs file. Uncomment wanted tests as necessary. 
```

## Maintainers

Rick Schnellert, Daniel Ruegenberg


## License

MIT © 2022 Daniel Ruegenberg
