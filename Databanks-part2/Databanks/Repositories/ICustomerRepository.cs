﻿using Databanks.Model;
using System.Collections.Generic;


namespace Databanks.Repositories
{
    public interface ICustomerRepository
    {
        public Customer GetCustomer(int id);
        public List<Customer> GetCustomerByName(string name);
        public List<Customer> GetAllCustomers();
        public List<Customer> GetAllCustomers(int limit, int offset);
        public bool AddNewCustomer(Customer test);
        public bool UpdateCustomer(Customer customer);
        public bool DeleteCustomer(int id);
        public List<Countrys> CustomerPerCountry();
        public List<Customer> HighestSpender();
        public List<Customer> GetCustomerGenre(int id);
    }
}
