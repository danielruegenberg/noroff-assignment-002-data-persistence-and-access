﻿using Databanks.Model;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;

namespace Databanks.Repositories
{
    public class CustomerRepository : ICustomerRepository
    {
        /// <summary>
        /// Returned a List of all Customers
        /// </summary>
        /// <returns>List<Customer></returns>
        public List<Customer> GetAllCustomers()
        {
            List<Customer> custList = new List<Customer>();
            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        using (SqlDataReader reader = cmd?.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Customer temp = new();
                                temp.CustomerId = reader.IsDBNull(0) ? 0 : reader.GetInt32(0);
                                temp.FirstName = reader.IsDBNull(1) ? null : reader.GetString(1);
                                temp.LastName = reader.IsDBNull(2) ? null : reader.GetString(2);
                                temp.Country = reader.IsDBNull(3) ? null : reader.GetString(3);
                                temp.PostalCode = reader.IsDBNull(4) ? null : reader.GetString(4);
                                temp.Phone = reader.IsDBNull(5) ? null : reader.GetString(5);
                                temp.Email = reader.IsDBNull(6) ? null : reader.GetString(6);
                                custList.Add(temp);
                            }
                        }
                    }
                }
            }catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return custList; 
        }
        /// <summary>
        /// Returns a Customer by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Customer</returns>
        public Customer GetCustomer(int id)
        {
            Customer customer = new();
            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer WHERE CustomerId = @CustomerId";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@CustomerId", id);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                customer.CustomerId = reader.IsDBNull(0) ? 0 : reader.GetInt32(0);
                                customer.FirstName = reader.IsDBNull(1) ? null : reader.GetString(1);
                                customer.LastName = reader.IsDBNull(2) ? null : reader.GetString(2);
                                customer.Country = reader.IsDBNull(3) ? null : reader.GetString(3);
                                customer.PostalCode = reader.IsDBNull(4) ? null : reader.GetString(4);
                                customer.Phone = reader.IsDBNull(5) ? null : reader.GetString(5);
                                customer.Email = reader.IsDBNull(6) ? null : reader.GetString(6);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return customer;
        }
        /// <summary>
        /// Returns Customers that partially match the name
        /// </summary>
        /// <param name="name"></param>
        /// <returns>List<Customer></returns>
        public List<Customer> GetCustomerByName(string name)
        {
            List<Customer> custList = new List<Customer>();
            name += "%";
            Customer customer = new();
            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer WHERE FirstName LIKE @name OR LastName LIKE @name";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@name", name);
                        using (SqlDataReader reader = cmd?.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Customer temp = new();
                                temp.CustomerId = reader.IsDBNull(0) ? 0 : reader.GetInt32(0);
                                temp.FirstName = reader.IsDBNull(1) ? null : reader.GetString(1);
                                temp.LastName = reader.IsDBNull(2) ? null : reader.GetString(2);
                                temp.Country = reader.IsDBNull(3) ? null : reader.GetString(3);
                                temp.PostalCode = reader.IsDBNull(4) ? null : reader.GetString(4);
                                temp.Phone = reader.IsDBNull(5) ? null : reader.GetString(5);
                                temp.Email = reader.IsDBNull(6) ? null : reader.GetString(6);
                                custList.Add(temp);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return custList;
        }
        /// <summary>
        /// adds a new Customer
        /// </summary>
        /// <param name="customer"></param>
        /// <returns>bool</returns>
        public bool AddNewCustomer(Customer customer)
        {
            bool succes = false;
            string sql = "INSERT INTO Customer(FirstName, LastName, Email) " + "VALUES(@FirstName,@LastName,@Email)";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@FirstName", customer.FirstName);
                        cmd.Parameters.AddWithValue("@LastName", customer.LastName);
                        cmd.Parameters.AddWithValue("@Email", customer.Email);
                        succes = cmd.ExecuteNonQuery() > 0 ? true : false; 
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return succes;
        }
        /// <summary>
        /// Updates Phone number of Customer, maybe improve to update any Columm
        /// </summary>
        /// <param name="customer"></param>
        /// <returns>bool</returns>      
        public bool UpdateCustomer(Customer customer)
        {
            bool succes = false;
            string sql = "UPDATE Customer SET Phone = @Phonenumber WHERE CustomerId = @Id";
            try    
            {
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@Phonenumber", customer.Phone);
                        cmd.Parameters.AddWithValue("@Id", customer.CustomerId);
                        succes = cmd.ExecuteNonQuery() > 0 ? true : false;
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return succes;
        }
        /// <summary>
        /// Deletes a Customer by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>bool</returns>
        public bool DeleteCustomer(int id)
        {
            bool succes = false;
            string sql = "DELETE Customer WHERE CustomerId = @Id";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@Id", id);
                        succes = cmd.ExecuteNonQuery() > 0 ? true : false;
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return succes;
        }
        /// <summary>
        /// Returns a List of Customers with a limit and an offset
        /// </summary>
        /// <param name="limit"></param>
        /// <param name="offset"></param>
        /// <returns>List<Customer></returns>
        public List<Customer> GetAllCustomers(int limit, int offset)
        {
            List<Customer> custList = new List<Customer>();
            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer ORDER BY CustomerId OFFSET @offset ROWS FETCH NEXT @limit ROWS ONLY";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@limit", limit);
                        cmd.Parameters.AddWithValue("@offset", offset);
                        using (SqlDataReader reader = cmd?.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Customer temp = new();
                                temp.CustomerId = reader.IsDBNull(0) ? 0 : reader.GetInt32(0);
                                temp.FirstName = reader.IsDBNull(1) ? null : reader.GetString(1);
                                temp.LastName = reader.IsDBNull(2) ? null : reader.GetString(2);
                                temp.Country = reader.IsDBNull(3) ? null : reader.GetString(3);
                                temp.PostalCode = reader.IsDBNull(4) ? null : reader.GetString(4);
                                temp.Phone = reader.IsDBNull(5) ? null : reader.GetString(5);
                                temp.Email = reader.IsDBNull(6) ? null : reader.GetString(6);
                                custList.Add(temp);
                            }
                        }
                    }
                }
            }catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return custList; 
        }
        /// <summary>
        /// Shows count of Customers per Country
        /// </summary>
        /// <returns>List<Countrys></returns>
        public List<Countrys> CustomerPerCountry()
        {
            List<Countrys> countryList = new List<Countrys>();
            string sql = "SELECT DISTINCT Country, COUNT(CustomerId) OVER (PARTITION BY Country) AS 'Count' FROM Customer ORDER BY 'Count' DESC";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        using (SqlDataReader reader = cmd?.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Countrys temp = new();
                                temp.Count = reader.IsDBNull(1) ? 0 : reader.GetInt32(1);
                                temp.Country = reader.IsDBNull(0) ? null : reader.GetString(0);
                                countryList.Add(temp);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return countryList;
        }
        /// <summary>
        /// Shows Customers ordered by highest total spending
        /// </summary>
        /// <returns>List<Customer></returns>
        public List<Customer> HighestSpender()
        {
            List<Customer> custList = new List<Customer>();
            string sql = "SELECT DISTINCT Invoice.CustomerId, Customer.FirstName, Customer.LastName, SUM(Invoice.Total) OVER (PARTITION BY Invoice.CustomerId) AS TotalSpending  FROM Invoice INNER JOIN Customer ON Customer.CustomerId = Invoice.CustomerId ORDER BY TotalSpending DESC";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        using (SqlDataReader reader = cmd?.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Customer temp = new();
                                temp.CustomerId = reader.IsDBNull(0) ? 0 : reader.GetInt32(0);
                                temp.FirstName = reader.IsDBNull(1) ? null : reader.GetString(1);
                                temp.LastName = reader.IsDBNull(2) ? null : reader.GetString(2);
                                temp.Spending = reader.IsDBNull(3) ? 0 : reader.GetDecimal(3);
                                custList.Add(temp);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return custList;
        }
        /// <summary>
        /// Shows a Customer by id and the Genre with the highest purchest music genre
        /// </summary>
        /// <param name="id"></param>
        /// <returns>List<Customer></returns>
        public List<Customer> GetCustomerGenre(int id)
        {
            List<Customer> custList = new List<Customer>();
            string sql = "SELECT DISTINCT Genre.\"Name\", Invoice.CustomerId, Customer.FirstName, Customer.LastName, COUNT(Genre.\"Name\") OVER (PARTITION BY Genre.\"Name\") AS \"Count\" FROM Genre INNER JOIN Track ON Genre.GenreId = Track.GenreId INNER JOIN InvoiceLine ON Track.TrackId = InvoiceLine.TrackId INNER JOIN Invoice ON InvoiceLine.InvoiceId = Invoice.InvoiceId INNER JOIN Customer ON Invoice.CustomerId = Customer.CustomerId WHERE Customer.CustomerId = @CustomerId ORDER BY \"Count\" DESC";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@CustomerId", id);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            Customer tempBefor = new();
                            while (reader.Read())
                            {
                                Customer temp = new();
                                temp.CustomerId = reader.IsDBNull(0) ? 0 : reader.GetInt32(1);
                                temp.FirstName = reader.IsDBNull(1) ? null : reader.GetString(2);
                                temp.LastName = reader.IsDBNull(2) ? null : reader.GetString(3);
                                temp.Genre = reader.IsDBNull(3) ? null : reader.GetString(0);
                                temp.GenreCount = reader.IsDBNull(4) ? 0 : reader.GetInt32(4);
                                if (tempBefor.GenreCount >= temp.GenreCount) break;
                                custList.Add(temp);
                                tempBefor = temp;
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return custList;
        }
    }
}
