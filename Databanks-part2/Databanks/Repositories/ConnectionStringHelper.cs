﻿using Microsoft.Data.SqlClient;

namespace Databanks.Repositories
{
    public class ConnectionStringHelper
    {
        /// <summary>
        /// Build Connection string and return it
        /// </summary>
        /// <returns>string</returns>
        public static string GetConnectionString()
        {
            SqlConnectionStringBuilder connectionStringBuilder = new SqlConnectionStringBuilder();
            connectionStringBuilder.DataSource = "RICK-PC\\SQLEXPRESS";
            connectionStringBuilder.InitialCatalog = "Chinook";
            connectionStringBuilder.IntegratedSecurity = true;
            connectionStringBuilder.TrustServerCertificate = true;
            return connectionStringBuilder.ConnectionString;
        }
    }
}
