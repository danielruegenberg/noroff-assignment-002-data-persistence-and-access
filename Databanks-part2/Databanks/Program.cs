﻿using Databanks.Model;
using Databanks.Repositories;
using System;
using System.Collections.Generic;

namespace Databanks
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Chinook Databank");

            ICustomerRepository repository = new CustomerRepository();
            TestGetAllCustomers(repository);
            //TestGetCustomerGenre(repository);
            //TestGetCustomer(repository);
            //TestGetCustomerByName(repository);
            //TestAddNewCustomer(repository);
            //TestUpdateCustomer(repository);
            //TestDeleteCustomer(repository);
            //TestCustomerPerCountry(repository);
            //TestHighestSpender(repository);
            //TestGetCustomerGenre(repository);
        }
        static void TestGetAllCustomers(ICustomerRepository repository)
        {
            PrintCustomers(repository.GetAllCustomers());
        }
        static void TestGetCustomer(ICustomerRepository repository)
        {
            PrintCustomer(repository.GetCustomer(1));
        }
        static void TestGetCustomerByName(ICustomerRepository repository)
        {
            PrintCustomers(repository.GetCustomerByName("Ri"));
        }
        static void TestAddNewCustomer(ICustomerRepository repository)
        {
            Customer test = new Customer();
            {
                test.FirstName = "Rick";
                test.LastName = "Schnellert";
                test.Email = "Rick@Rick.de";
            };
            if (repository.AddNewCustomer(test))
            {
                Console.WriteLine("Yay, insert worked");
            }
            else
            {
                Console.WriteLine("No, insert failed!");
            }
        }
        static void TestUpdateCustomer(ICustomerRepository repository)
        {
            Customer test = new Customer();
            {
                test.CustomerId = 61;
                test.Phone = "012345";                
            };
            if (repository.UpdateCustomer(test)) Console.WriteLine("Yay, update worked");
            else Console.WriteLine("No, update failed!");
        }
        static void TestDeleteCustomer(ICustomerRepository repository)
        {
            if (repository.DeleteCustomer(60)) Console.WriteLine("Yay, delete worked");
            else Console.WriteLine("No, delete failed!");
        }
        static void TestCustomerPerCountry(ICustomerRepository repository)
        {
            PrintCountrys(repository.CustomerPerCountry());
        }
        static void TestHighestSpender(ICustomerRepository repository)
        {
            PrintCustomersSpending(repository.HighestSpender());
        }
        static void TestGetCustomerGenre(ICustomerRepository repository)
        {
            PrintCustomerGenre(repository.GetCustomerGenre(2));
        }
        static void PrintCustomer(Customer customer)
        {
            Console.WriteLine($"--- { customer.CustomerId} { customer.FirstName} {customer.LastName} -- { customer.Country} -- { customer.PostalCode} -- { customer.Phone} -- {customer.Email}  ---");
        }
        
        static void PrintCustomers(List<Customer> customers)
        {
            customers.ForEach(customer =>
            {
                Console.WriteLine($"--- { customer.CustomerId} { customer.FirstName} {customer.LastName} -- { customer.Country} -- { customer.PostalCode} -- { customer.Phone} -- {customer.Email} ---");
            });
        }
        static void PrintCustomersSpending(List<Customer> customers)
        {
            customers.ForEach(customer =>
            {
                Console.WriteLine($"--- { customer.CustomerId} { customer.FirstName} {customer.LastName} : { customer.Spending} ---");
            });
        }
        static void PrintCustomerGenre(List<Customer> customers)
        {
            customers.ForEach(customer =>
            {
                Console.WriteLine($"--- { customer.CustomerId} { customer.FirstName} {customer.LastName} : { customer.Genre} -- { customer.GenreCount} ---");
            });
        }
        static void PrintCountrys(List<Countrys> country)
        {
            country.ForEach(country =>
            {
                Console.WriteLine($"--- { country.Country} : { country.Count} ---");
            });
        }
    }
}
 