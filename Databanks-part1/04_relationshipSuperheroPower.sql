--Creating link table linkSuperheroPower with table Superhero primary key and table Power primary key as foreign keys

USE SuperheroesDb

--Create table linkSuperheroPower with columns superheroKey and powerKey
CREATE TABLE linkSuperheroPower(
	superheroKey int NOT NULL,
	powerKey int NOT NULL,
	PRIMARY KEY (superheroKey, powerKey)
)

--Setting up superheroKey as foreign key on table Superhero ID an powerKey as foreign key on table Power ID
ALTER TABLE	linkSuperheroPower
ADD CONSTRAINT relationshipSuperheroLink FOREIGN KEY (superheroKey)
REFERENCES Superhero (ID)

ALTER TABLE	linkSuperheroPower
ADD CONSTRAINT relationshipPowerLink FOREIGN KEY (powerKey)
REFERENCES "Power" (ID)