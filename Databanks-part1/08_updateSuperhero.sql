--Updating name and origin of superhero Ant-Man

USE SuperheroesDb

UPDATE Superhero
SET 
"Name" = 'Scott Lang',
Origin = 'Coral Gables'
WHERE Alias = 'Ant-Man'