--Create the Superhero, Assistant and Power table and setup their primary keys

USE SuperheroesDb

CREATE TABLE Superhero (
	
	ID int IDENTITY(1,1) PRIMARY KEY NOT NULL,
	"Name" varchar(50) NOT NULL,
	Alias varchar(50) NOT NULL,
	Origin varchar(50) NOT NULL
)

CREATE TABLE Assistant (
	
	ID int IDENTITY(1,1) PRIMARY KEY NOT NULL,
	"Name" varchar(50) NOT NULL
)

CREATE TABLE "Power" (
	
	ID int IDENTITY(1,1) PRIMARY KEY NOT NULL,
	"Name" varchar(50) NOT NULL,
	"Description" varchar(150) NOT NULL
)