--Create new column superheroID in table Assistant and set it up as foreign key on table Superhero ID 

USE SuperheroesDb

--Creating the column superheroID in table Assistant
ALTER TABLE	Assistant
ADD superheroID int

--Setting up column superheroID in table Assistant as foreign key on table Superhero, column ID
ALTER TABLE	Assistant
ADD CONSTRAINT relationshipSuperheroAssistant FOREIGN KEY (superheroID)
REFERENCES Superhero (ID)