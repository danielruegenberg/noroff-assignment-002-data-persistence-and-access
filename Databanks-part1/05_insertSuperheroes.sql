--Adding superheroes to the Superhero table

USE SuperheroesDb

INSERT INTO Superhero ("Name", Alias, Origin)
VALUES
('Peter Parker', 'Spiderman', 'New York'),
('Bruce Wayne', 'Batman', 'Gotham City'),
('Henry Jonathan Pym', 'Ant-Man', 'Unkown')
