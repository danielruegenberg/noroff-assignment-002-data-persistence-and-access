--Adding powers to the Powers table and assign them to heroes

USE SuperheroesDb

--Adding powers to Power table
INSERT INTO "Power" ("Name", "Description")
VALUES
('Superhuman Strength', 'Is stronger than any normal human could be'),
('Supreme Technology', 'Can use technology, that is far more advanced than technology any normal human has access to'),
('Spider Web', 'Can create spider webs to swing around buildings or attack enemies'),
('Size Manipulation', 'Can change his own size from atomic level to giant')

--Linking powers to superheroes
INSERT INTO linkSuperheroPower (superheroKey, powerKey)
VALUES
(1, 1),
(1, 3),
(2, 2),
(3, 1),
(3, 2),
(3, 4)